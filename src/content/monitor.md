---
layout: post
title: How To Monitor Containerized Application
image: img/prom.JPG
author: [Shivam]
date: 2019-10-19T07:03:47.149Z
tags: ['Getting Started']
draft: false
---

Monitoring is a crucial aspect of any DevOps pipeline and for technologies like Kubernetes which is a rage right now, a robust monitoring setup can bolster your confidence to migrate production workloads from VMs to Containers and a tool, prometheus was created to monitor highly dynamic containerized environments/applications.

> Today we will deploy a Prometheus based monitoring system using Lens and Kubernetes Operator.

---
## Why Monitoring?
---
> Modern DevOps is becoming more and more complex to be handled manually and therefore need for automation arose.

System serving millions of requests where you have multiple servers that run containerized applications and these tons of processes, applications, servers are all interconnected, and maintaining such a setup to run smoothly without application downtimes is very challenging. Manually debugging a crash can be a very time-consuming and laborious task.

---
### Let's Define a Use Case
---

Suppose you have 100s of microservices running where one specific server ran out of memory and kicked off a running container that was responsible for providing database sync between two database pods in a Kubernetes cluster, that in turn caused those two database servers to fail and that database was used by an authentication service that also stopped working because database became unavailable and then the application that depends on that authentication services couldn’t authenticate users in UI anymore and user will be prompted with **Sign-In Errors**

> What would be helpful in this situation is to constantly monitor services if they are up and running and immediately alerts the maintainers about the crashes. So you know exactly what and when happened? and even it identifies the problem way before it actually occurs and alerts the system admins to prevent that issue.

---
## How does Prometheus architecture look like?
---

![Prometheus Architecture](https://i.ibb.co/BGfH7H4/promarch.png)

#### Let’s talk about its core component, first:

Prometheus server At its core, it has a Prometheus server that does the actual monitoring work, and is made up of three parts:

- **Data Retrieval Worke**r: Responsible for pulling metrics data from applications, servers, services, and other target resources and storing them into the database.

- **Time Series DataBase**: It stores all the metrics data like current CPU usage or no. of exceptions in an application.

- **HTTP Serve**r: A server or web API that accepts queries for stored data and server API is used to display the data in a dashboard or a UI through either a Prometheus dashboard or any other data visualization tool like Grafana.

---
## What does Prometheus monitor?
---

It monitors a particular thing which can be a Linux/Windows server, Nginx server, single application, or service like a database. The things that Prometheus monitors are called Targets and each target has Units of monitoring.

> For example, if the target is a python application, units can be no. of requests, no. of exceptions, errors. Or if the target is a Linux server, units can be current CPU status, memory usage, disk space.

---
## How do metrics collect data?
---
Prometheus pulls data from the targets (say, your application, link server, Nginx, Redis) from an HTTP endpoint which by default hosted at hostaddress/metrics. For this to work a target must expose/metrics endpoint and data available at the metrics endpoint must be in the format that Prometheus understands as we have in this picture:

![Prometheus Requests](https://i.ibb.co/4TtnPjr/Capture.jpg)

---
## How do I expose these metrics?
---

Prometheus has a list of exporters here: [Exporters](https://prometheus.io/docs/instrumenting/exporters/)
which is required to expose the metrics endpoint from services. It is a script or a service that-

1. Fetches metrics from the target(say, A python application)
2. Converts them in a format that Prometheus understands.
3. Exposes them on an endpoint /metrics where Prometheus can scrape them.

---
## How To SetUp Prometheus?
---

1. Download The Lens Dashboard from [Here](https://k8slens.dev/)
2. Install the Program.
3. Add the Kubernetes Cluster to the Lens to authenticate using KubeConfig File Like - 

![Lens](https://i.ibb.co/gvYzGZm/Capture.jpg)

4. Go to Cluster Settings and Click **Install Prometheus** with Operator.

Your Cluster Monitoring is deployed and ready to be exported to visualization tools like Grafana.

![Prometheus](https://i.ibb.co/wh5rLNt/Capture.jpg)


