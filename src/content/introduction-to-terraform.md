---
layout: post
title: Introduction To Terraform
image: img/terraform.png
author: [Shivam]
date: 2019-06-30T07:03:47.149Z
tags:
  - DevOps
draft: false
---

Infrastructure-as-Code (IAC) is a Thing now. CI’s now are not only limited to Code. ‘I’ in CI was missing a critical meaning : "Infrastructure".

---

![DevOps](img/devops.png)

There was a time when Infrastructure either meant Large Server Rooms or all components deployed in the public cloud before even testing the requirements for the software.

With the Introduction of IAC, infrastructure management is now just a Push away. The automation of infrastructure deployment has now become an integral part of DevOps which creates insanely faster release cycles.

---
> The use of continuous delivery pipelines for infrastructure to orchestrate the release cycle is now an essential part of DevOps.
IAC Pipeline is advantageous to find errors even before the changes are applied to environments.

Before proceeding further,

> If you are here, I assume you have the basic knowledge of Public Clouds like Azure, AWS, GCP. I will be using AWS for the sake of this article.

## Steps To Follow
---
The purpose of this article is to make you understand and appreciate the problem that is being solved with the IAC pipeline. You’ll need to Signup to use terraform here. Make sure you get some ideas by reading the intro article here, this will accelerate you in no time.

Like any other programming language, terraform also has Variable declarations and Best Practice recommends that Terraform script should have different files for variables e.g. variable.tf or .tfvars. But for now, we don’t need to worry about that.

Terraform can be used either by downloading the packages locally or by using Terraform Cloud, an online dashboard (GUI Version). For this article, we will be using Terraform Cloud. It’s better to get a basic understanding of terraform before downloading and using it as CLI.

---

Let’s code a basic script to get things started:

```hcl
#For AWS
provider "aws" {
  region     = "us-east-1"
  access_key = ""
  secret_key = ""
}
```
Steps To Configure IAC Pipeline
Step 1 : Create a Repository on Github with Authentication Script above respective to your provider.

Step 2 : Log-in to your Terraform cloud dashboard and create an Organization.

Organizations are a shared space for teams to collaborate on workspaces in Terraform Cloud

Step 3 : Creating a workspace —
Workspaces are how Terraform Cloud organizes infrastructure. A workspace consists of:

- A collection of Terraform configurations (retrieved from a VCS repo).
- Values for any variables those configurations require.
- Persistent stored state for the resources it manages.
- Historical state and run logs.

Step 3.1 : Configure VCS (Github.com in this case)

Step 4 : Configure Variables

<b> Note: Your keys will be used here as secret variables, Terraform will treat these variables as secrets.</b>

Step 5 : Update and commit anything like, add comment or change the location in the provider. After the Commit terraform plan will be triggered and will automatically perform the tasks as written in the terraform scripts, in this case, Authenticate to your cloud provider.

If your planning was successful, Congratulations you have successfully authenticated to your cloud provider.
